"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include, url
#añadí esta linea porque las instrucciones indicaban un url
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'', include('blog.urls')),
    #ingresé path aunque en las indicaciones del ejercicio dice url. Entiendo que path se incorporó en django posterior a versión 2.
    #luego ingresé url porque leí que la sintaxis es diferente entre path y url
]
